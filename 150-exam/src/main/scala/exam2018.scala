// Name: _____________
// ITU email: ________
package adpro.exam2018

import fpinscala.monoids.Monoid
import fpinscala.monads.Monad
import fpinscala.monads.Functor
import fpinscala.laziness.{Stream,Empty,Cons}
import fpinscala.laziness.Stream._
import fpinscala.parallelism.Par._
import scala.language.higherKinds
import adpro.data._
import adpro.data.FingerTree._
import monocle.Lens

object Q1 { 
  def groupByKey[K,V] (l :List[(K,V)]) :List[(K,List[V])] = {
    l.foldLeft (Map[K, List[V]]()) ((acc, tuple) => acc.get(tuple._1) match {
      case Some(l) => acc.updated(tuple._1, tuple._2 :: l);
      case None => acc.updated(tuple._1, List(tuple._2))
    }).toList
  }
}


object Q2 { 
  def f[A,B] (results: List[Either[A,B]]) :Either[List[A],List[B]] = {
    val errs = results collect { case Left(v) => v};
    if(errs.size == 0) {
      // If no errors
      // Right(results.map(el => el.right.get))
      Right(results collect { case Right(v) => v})
    } else {
      Left(errs)
    }
  }
}


object Q3 {

  type T[B] = Either[String,B]
  implicit val eitherStringIsMonad: Monad[T] = new Monad[T] {
    override def unit[B] (b: => B) = Right(b);
    override def flatMap[B,C] (either: Either[String, B]) (f: B => Either[String, C]): Either[String, C] = either flatMap f
  }



  implicit def eitherIsMonad[A] = {
    type T[B] = Either[A,B]
    new Monad[T]{
      override def unit[B] (b: => B) = Right(b);
      override def flatMap[B,C] (either: Either[A, B]) (f: B => Either[A, C]): Either[A, C] = either flatMap f
    }
  }

} // Q3


object Q4 {

   // Write the answers in English below.
   
   // A. ...
   
   // B. ...
   
}


object Q5 { 

  def parForall[A] (as: List[A]) (p: A => Boolean): Par[Boolean] = ???

}


object Q6 {

  def apply[F[_],A,B](fab: F[A => B])(fa: F[A]): F[B] = ???
  def unit[F[_],A](a: => A): F[A] = ???

  val f: (Int,Int) => Int = _ + _
  def a :List[Int] = ???

  // val x = apply (apply (unit (f.curried)) (a)) (a)
  // Answer below in a comment:

  // ...

} // Q6


object Q7 {

  def map2[A,B,C] (a :List[A], b: List[B]) (f: (A,B) => C): List[C] =  a flatMap (el1 => b map (el2 => f(el1, el2)))


  def map3[A,B,C,D] (a :List[A], b: List[B], c: List[C]) (f: (A,B,C) => D) :List[D] = 
    map2 (map2 (a, b) { case (a,b) => (a,b)}, c) { case ((a,b),c) => f(a,b,c)}


  // def map3monad ...
  def map3monad[M[_]: Monad, A,B,C,D] (a: M[A], b: M[B], c: M[C]) (f: (A, B, C) => D): M[D] = 
    implicitly[Monad[M]].map2 (
      implicitly[Monad[M]].map2 (a, b) { case (a,b) => (a,b)}, c) { case ((a,b),c) => f(a,b,c)}
} // Q7


object Q8 {

  def filter[A] (t: FingerTree[A]) (p: A => Boolean): FingerTree[A] = ???

}


object Q9 {

  def eitherOption[A,B] (default: => A): Lens[Either[A,B], Option[B]] = Lens[Either[A,B], Option[B]] (either => either match {
    case Right(v) => Some(v)
    case _ => None
  }) (
    opt => _ => opt match {
      case Some(v) => Right(v)
      case _ => Left(default)
    }
  ) 


  // Answer the questions below:

  // A. ...

  // B. ...

  // C. ...

} // Q9

